from pygments import highlight
from pygments.lexers import get_lexer_by_name, get_all_lexers
from pygments.formatters import HtmlFormatter


def pygmentsHighlighter(code, lang="python", lines=False):
    lexer = get_lexer_by_name(lang, stripall=False)
    formatter = HtmlFormatter(cssclass="highlight", linenos=lines)
    result = '<style type="text/css">' + pygmentsGetStyle() + '</style>'
    result += highlight(code, lexer, formatter)
    return result



def pygmentsGetStyle():
    return HtmlFormatter().get_style_defs('.highlight')

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////home/drivedric/python/paste2py/db/db.sql"
db = SQLAlchemy(app)

class Paste(db.Model):
    id = db.Column(db.Integer, primary_key = True, unique=True)
    language = db.Column(db.String(100))
    paste = db.Column(db.Text)
    user = db.Column(db.String(150))
    title = db.Column(db.String)
    create_date  = db.Column(db.String)

    def __init__(self, language, paste, user, title):
        self.language = language
        self.paste = paste
        self.user = user
        self.title = title
        self.create_date = str(datetime.utcnow()).split('.')[0] 


        

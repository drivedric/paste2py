#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 

Paste2py - A Python pastebin using Flask, Pygments, Jinja2 and SQLAlchemy   

version: 1.0.0
author: drivedric <drivedric@gmail.com>
website: http://paste2py.drivedric.com 

You can fork this project at https://github.com/drivedric/paste2py.git 

"""

from pygments.lexers import get_all_lexers
from pygments_func.pygmentsHighlight import pygmentsHighlighter, pygmentsGetStyle
from db.databaseManager import db, Paste
from flask import Flask, request, make_response, redirect, url_for, abort, render_template

app = Flask(__name__)

@app.route('/', methods = ["GET", "POST"])
def paste():
    """ Send a paste !  """    
    if request.method == "GET":  # the form is not send, show the form
        lang = [x for x in sorted(get_all_lexers())] # all the languages for <select>      
        return render_template("send_form.tpl", lang=lang)

    else: # the method is POST, the form is send
        data = request.form # data['data'] is shorter than request.form['data']
        
        # check if fields is empty
        if not data['user'] ==  "" and not data['title'] == "" and not data['language'] == "" and not data['paste'] == "":
            # nothing is empty, we send the paste in database
            new_paste = Paste(data['language'], data['paste'], data['user'], data['title'])
            db.session.add(new_paste)
            db.session.commit()
            
            # get id for redirect to paste
            return redirect('/paste/{}'.format(Paste.query.all()[-1].id)) 
        
        else: # one of the fields is empty 
            return render_template('error_paste_send.tpl') # error, one or some of the fields is empty   



@app.route('/paste/<id>')
def read_paste(id):
    """ 
    Show the paste of a user from the id 
    You can easily set some features directly from url like :
        - the language to use
        - if you want to active lines or not
        - raw paste
        - ... 
 
    """

    # check if <id> is a valid number
    if not isinstance(id, int):
        try:
            id = int(id)
        except ValueError:
            return abort(404)
    
   
    data = Paste.query.filter_by(id=id).first()
    
    # check if <id> exist
    if data is None: 
        abort(404) 
 
    # check settings in url 
    language = request.args['lang'] if request.args.get('lang') is not None else data.language
    lines = 'table' if request.args.get('ln') is not None else False
    
    # check if ``raw`` option is active
    if request.args.get('raw') is not None:
        return data.paste
    else: # else we highlight the code
        return render_template('paste.tpl', code=pygmentsHighlighter(data.paste, language, lines))



@app.route('/usr/')
def usr():
    """ A list of users for check the paste of a user """
    users = Paste.query.all() # get all in db
    users_list = []
    for user in users: # and add the username in a list (no repetition)
        if user.user in users_list:
            continue
        else:
            users_list.append(user.user)
        
    return render_template('users_list.tpl', users_list=users_list) # then show the list of users 



@app.route('/usr/<username>')
def usr_pastes(username):
    """ A list of the pastes of a user """
    if username == "":
        abort(404)
  
    list_of_pastes = Paste.query.filter_by(user=username) # List of object Paste
    
    try:  # check if user exist
        isinstance(list_of_pastes[0], Paste)
    except IndexError: # no Paste object, no user, no pastes ! 
        abort(404)  

    return render_template('user_pastes.tpl', list_of_pastes=list_of_pastes)
  


@app.route('/doc/')
def doc():
    """ Documentation of Paste2py """
    return render_template("doc.tpl")



@app.errorhandler(404)
@app.route('/404')
def error404(error=None): # error=None, else some trouble with errorhandler (1 arg = error code ) and route (no arg)
    """ The page for 404 error """
    return make_response("The page you are looking for does not exist", 404)



if __name__ == "__main__":
    app.run(debug=True) 

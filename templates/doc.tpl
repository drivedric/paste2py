{% include "header.tpl" %}
 <style>
    pre {
        background: #000000;
        color: #ffffff;
    }
    .background {
         display: inline;
         color: #ffffff;
         background: #000000;
    }

    div {
        margin-left: 20px;
    }
    
    h3 {
        margin-left: 10px;
    }

    p {
        font-size: 95%;
        font-color: #303030;
    }
</style>
   <h1>Documentation</h1>
      <p>
       Welcome to the documentation of Paste2py!
        <br />
       Did you say a documentation for a pastebin tool? Yes! Paste2py need a documentation because of some hidden features integrated in the pastebin. Are you ready for reading? Here' we go!
      </p>
          <ul>
             <li>
                <a href="#features">What about these hidden features?</a>
                   <ul>
                      <li>Change the language use to hihlight</li>
                      <li>Enable or disable the lines</li>
                      <li>Enable the "raw" mode</li>
                   </ul>
              </li>

              <li>
                 <a href="#users">Retrieve the pastes of a user</a> 
                    <ul>
                       <li>Access to the list of users</li>
                        <li>Access to the pastes of a user</li>
                    </ul>
              </li>              
            
              <li>
                 <a href="#script">Use paste2.py script to paste from command line</a>
                    <ul>
                       <li>Download paste2.py command line script</li>
                       <li>How to use it?</li>
                    </ul>
               </li>
               
               <li>
                  <a href="#about">About paste2py</a>
                     <ul>
                        <li>How paste2py work?</li>
                        <li>Who maintain paste2py?</li>
                        <li>Can I contribute?</li>
                     </ul>
               </li>
              
          </ul>
<div>      
    <h2 id="features">What about there hidden features?</h2>
        <p>
            <h3>Change the language use to highlight</h3>
                You have entered the bad language or you just want to change it? Nothing easily! You just have to add <span class="background">?lang=[the_language]</span> in the url, just after the id!
            <h3>Enable or disable the lines</h3>
                Hum... This is a long paste, I'd love to have lines..., you say? You can! You just have to add <span class="background">?ln</span> in the url, after the id or the <span class="background">?lang</span> if you had add it.
            <h3>Enable the raw-mode</h3>
                You want to download the paste with <span class="background">wget</span> or something like that? Don't take the HTML with it!, add <span class="background">?raw</span> at the end of the url!
        </p>
      <br /><br />
    <h2 id="users">Retrieve the pastes of a user</h2>
        <p>
            This feature is useful if you want to retrive the past of a user or something like that.
            <h3>Access to the list of users</h3>
                You want to find the pastes of a user? Just go to /usr/, and you wanna see a list of the users
            <h3>Access to the pastes of a user</h3>
                Right, you know how to access to the list of users, but it's more useful to see the user's pastes! Lets go to /usr/your_username to enjoy the list of your pastes!
        </p>
      <br /><br />
    <h2 id="script">Use paste2.py script to paste from command line</h2>
        <p>
            You are too lazy to: 1) open your browser 2) go to paste2py 3) use the web interface to paste? You'll love our command line script that allow you to paste on paste2py without do more than <pre>paste2.py your_username paste_title language paste_file</pre>
        <h3>Download paste2.py command line script</h3>
            To download paste2py command line script, just type in a terminal:
                <pre>curl https://raw.github.com/drivedric/paste2py/master/paste2-script.py > paste2py && chmod +x paste2py</pre>
        <h3>And to use it?</h3>
             Nothing easily! Just use:
                 <pre>paste2py username title language file_to_paste</pre>
 
        </p>
     <br /><br />
    <h2 id="about">About paste2py</h2>
        <p>
            <h3>How paste2py work?</h3>
            <h3>Who maintain paste2py?<h3>
            <h3>Can I contribute?</h3>
        </p> 

</div> 
{% include "footer.tpl" %}


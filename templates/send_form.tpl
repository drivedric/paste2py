{% include "header.tpl" %}


<h1>Paste2py</h1>
   <h3> Send a paste ! </h3>

<form method="post" action="">
   <label for="user">User: </label> 
      <input type="text" name="user" value="Anonymous"  id="user" /><br /><br />

   <label for="title">Title: </label>
      <input type="text" name="title" /><br /><br />
      
   <label for="language">Language: </label>
      <select name="language" id="language">
         {% for language in lang %}
             <option value="{{ language[1][0] }}">{{ language[0] }}</option> 
         {% endfor %} 
      </select><br /><br />

   <label for="paste">Paste: </label><br /><br />
      <textarea name="paste" id="paste" cols="100" rows="30"></textarea><br /><br />
   

   <input type="submit" value="Paste it !" />

</form>


{% include "footer.tpl" %} 

<!doctype html>
 <html lang="en">
   <head>
      <meta charset="utf-8">
      <link rel="stylesheet" href="{{ url_for('static', filename='design.css') }}" />
      
      <meta name="keywords" content="python pastebin, pastebin python, python, pastebin"> 
      <meta name="description" content="paste2py - A Python pastebin using Flask, Pygments, Jinja2 and SQLAlchemy">
      <meta name="robots" content="index, follow">

      <title>Paste2py - A Python pastebin using Flask, Pygments, Jinja2 and SQLAlchemy !</title>
   </head>
<body>

<header>
    <nav>
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/doc/">Documentation</a></li>
        </ul>
    </nav>
</header>
